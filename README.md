# RETRO CART KIT Installation Script #

This script will install and configure all the settings needed to get started with your gaming experience.

### Installation ###

1. Once RetroPie is setup you can press start on your controller and select quit
1. Select Quit Emulationstation which will bring you to command line.
1. enter `git clone git@bitbucket.org:djcottrell/retro-cart-kit.git`
1. when ready enter the folowing `./retro-cart-kit/install`
